package sk.chemat.vaadin.custom;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextField;

/**
 * Vytvarat vlastne komponenty vo Vaadin 14 ide niekolkymi sposobmi. Treba si pozriet https://vaadin
 * .com/docs/v14/flow/creating-components/creating-components-overview.html
 *
 * @author Daniel Dubec, 11/7/2019
 */
public class TextfieldWithButton extends TextField {

    public TextfieldWithButton() {
        Button btnClear = new Button(new Icon(VaadinIcon.CLOSE_BIG), e -> setValue(""));

        Button btnList = new Button(new Icon(VaadinIcon.ELLIPSIS_DOTS_V), e -> {
            ComboBox<String> cbo = new ComboBox<>("Počet", "10", "20", "30", "50", "100");
            Dialog d = new Dialog(cbo);
            cbo.addValueChangeListener(event -> {
                setValue(event.getValue());
                d.close();
            });
            d.open();

        });

        addToSuffix(btnClear, btnList);
    }
}
