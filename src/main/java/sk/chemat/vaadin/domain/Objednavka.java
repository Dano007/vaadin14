package sk.chemat.vaadin.domain;

import java.time.LocalDate;

/**
 * @author Daniel Dubec, 11/7/2019
 */
public class Objednavka {
    private LocalDate datum;
    private String predmet;
    private Integer pocet;

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(final LocalDate datum) {
        this.datum = datum;
    }

    public String getPredmet() {
        return predmet;
    }

    public void setPredmet(final String predmet) {
        this.predmet = predmet;
    }

    public Integer getPocet() {
        return pocet;
    }

    public void setPocet(final Integer pocet) {
        this.pocet = pocet;
    }
}
