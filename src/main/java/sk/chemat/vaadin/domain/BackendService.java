package sk.chemat.vaadin.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Vracia potrebne data pre {@link sk.chemat.vaadin.views.ObjednavkyGrid}
 *
 * @author Daniel Dubec, 11/7/2019
 */
public class BackendService {
    public List<Objednavka> getObjednavky() {

        final List<Objednavka> list = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Objednavka obj = new Objednavka();
            obj.setDatum(LocalDate.now().plusDays(i));
            obj.setPredmet("Objednávka " + i);
            obj.setPocet(i + 1);

            list.add(obj);
        }

        return list;
    }
}
