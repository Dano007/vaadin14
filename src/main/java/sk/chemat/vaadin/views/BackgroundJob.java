package sk.chemat.vaadin.views;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import java.util.function.Consumer;

/**
 * @author Daniel Dubec, 11/8/2019
 */
public class BackgroundJob {
    private UI ui;

    public BackgroundJob(final UI ui) {
        this.ui = ui;
    }


    /**
     * Spusti dlhotrvajucu ulohu na pozadi
     */
    public void start() {
        JobThread job = new JobThread();
        job.setStartListener(b -> startNotification());
        job.setStopListener(b -> stopDialog(b));

        job.start();
    }


    private void startNotification() {
        ui.access(() -> {
            Notification.show(
                    "Úloha na pozadí bola spustená. Pokračujte ďaľej v práci. Po dokončení úlohy budete upozornený.");
        });

    }

    private void stopDialog(final Boolean error) {
        ui.access(() -> {
            Dialog d = new Dialog();
            d.setCloseOnOutsideClick(false);

            Label label = new Label(error ? "Úloha na pozadí zlyhala" : "Úloha na pozadí bola úspešne dokončená");
            Button btnOpen = new Button("Otvor dokument", event -> {
                Label doc = new Label("Čína, ktorá má podobne ako okrem iného viaceré východoázijské krajiny " +
                        "problémy so " +
                        "závislosťou na hrách, prijala nové pravidlá obmedzujúce hranie detí a mladých ľudí do veku 18 rokov.\n" +
                        "\n" +
                        "Upozornili na to noviny The New York Times.\n" +
                        "\n" +
                        "Pravidlá sú najmä pre prevádzkovateľov online hier, ktoré sú v súčasnosti hrávané najčastejšie.\n" +
                        "\n" +
                        "Podľa pravidiel sa mladí vôbec nemôžu hrať od 22:00 do 8:00, cez pracovný deň sa môžu hrať maximálne 90 minút a v deň voľna maximálne 3 hodiny.\n" +
                        "\n" +
                        "Obmedzené je aj koľko peňazí môžu minúť v hrách na nákupy. Deti od 8 do 16 rokov môžu mesačne minúť maximálne 200 juanov, v prepočte cca 26 eur, a od 16 do 18 rokov 400 juanov, v prepočte cca 52 eur.");

                Notification n = new Notification();

                n.addThemeVariants(error ? NotificationVariant.LUMO_ERROR : NotificationVariant.LUMO_SUCCESS);
                n.setDuration(0);
                n.setPosition(Notification.Position.MIDDLE);

                Button btnClose = new Button("OK", e -> n.close());
                n.add(doc, btnClose);
                n.open();

                d.close();
            });
            btnOpen.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

            VerticalLayout content = new VerticalLayout(label, btnOpen);

            d.add(content);
            d.open();
        });
    }

    private class JobThread extends Thread {
        private Consumer<Boolean> startListener;
        private Consumer<Boolean> stopListener;

        public JobThread() {
            super();
        }

        @Override
        public void run() {
            try {
                if (startListener != null)
                    startListener.accept(false);
                // Urob nieco
                Thread.sleep(15000);

                if (stopListener != null)
                    stopListener.accept(false);
            } catch (Exception e) {
                e.printStackTrace();
                if (stopListener != null)
                    stopListener.accept(true);
            }
        }

        public void setStartListener(Consumer<Boolean> startListener) {
            this.startListener = startListener;
        }

        public void setStopListener(Consumer<Boolean> stopListener) {
            this.stopListener = stopListener;
        }
    }
}
