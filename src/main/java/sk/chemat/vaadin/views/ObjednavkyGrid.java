package sk.chemat.vaadin.views;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import sk.chemat.vaadin.domain.BackendService;
import sk.chemat.vaadin.domain.Objednavka;

/**
 * @author Daniel Dubec, 11/7/2019
 */
public class ObjednavkyGrid extends VerticalLayout {
    Grid<Objednavka> grid;

    public ObjednavkyGrid() {
        grid = new Grid<>();
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER,
                GridVariant.LUMO_NO_ROW_BORDERS);
        grid.setColumnReorderingAllowed(true);
        grid.addItemClickListener(e -> otvorEditor(e.getItem()));

        // Columns
        grid.addColumn(Objednavka::getPredmet)
                .setHeader("Predmet")
                .setSortable(true)
                .setId("#predmet");

        grid.addColumn(Objednavka::getDatum).setHeader("Datum");
        grid.addColumn(Objednavka::getPocet).setHeader("Pocet");

        grid.addColumn(new ComponentRenderer<>(obj -> {
            H3 h3 = new H3(obj.getPredmet());
            Label lbl = new Label("Dátum: " + obj.getDatum() + " " + obj.getPocet());
            Div div = new Div(h3, lbl);
            return div;
        }));


        // Data
        BackendService service = new BackendService();
        grid.setItems(service.getObjednavky());

        addAndExpand(grid);
    }

    /**
     * Otvori editor objednavky
     *
     * @param objednavka {@link Objednavka}
     */
    private void otvorEditor(Objednavka objednavka) {
        ObjednavkaEditor editor = new ObjednavkaEditor(objednavka);
        editor.addButtonZatvor(o -> editor.close());
        editor.addButtonUloz(o -> {
            //TODO Ulozit editovany objekt do db.
            grid.getDataProvider().refreshItem(objednavka);
            editor.close();
        });

        editor.open(false);
    }
}
