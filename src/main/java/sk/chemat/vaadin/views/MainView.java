package sk.chemat.vaadin.views;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;

/**
 * The main view contains a button and a click listener.
 */
@Route("")
@Push
@PWA(name = "Project Base for Vaadin", shortName = "Project Base")
public class MainView extends Div {

    public MainView() {
        Button btnGrid = new Button("Zobraz grid", e -> add(new ObjednavkyGrid()));
        Button btnBackgroundJob = new Button("Spusti úlohu na pozadí", e -> {
            BackgroundJob job = new BackgroundJob(UI.getCurrent());
            job.start();
        });

        add(btnGrid, btnBackgroundJob);
    }
}
