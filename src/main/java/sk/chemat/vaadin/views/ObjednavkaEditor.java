package sk.chemat.vaadin.views;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.StringToIntegerConverter;
import sk.chemat.vaadin.custom.TextfieldWithButton;
import sk.chemat.vaadin.domain.Objednavka;

import java.util.function.Consumer;

/**
 * Editor konkretnej objednavky {@link Objednavka}
 *
 * @author Daniel Dubec, 11/7/2019
 */
public class ObjednavkaEditor extends VerticalLayout {


    private final Binder<Objednavka> binder;
    private Objednavka editBean;
    private HorizontalLayout buttonLayout;
    private Dialog dialog;

    public ObjednavkaEditor(final Objednavka objednavka) {
        /*
          TODO Nacitat editovany objekt z DB, aby sme mali najaktualnejsiu verziu.
         */
        editBean = objednavka;

        DatePicker datum = new DatePicker("Datum");
        TextField predmet = new TextField("Predmet");
        predmet.setPlaceholder("Predmet objednavky");
        TextfieldWithButton pocet = new TextfieldWithButton();

        binder = new Binder<>(Objednavka.class);
        binder.forField(datum).asRequired().bind("datum");
        binder.forField(predmet).bind("predmet");
        binder.forField(pocet)
                .withConverter(new StringToIntegerConverter("Zly format cisla"))
                .bind("pocet");

        binder.readBean(editBean);

        FormLayout form = new FormLayout(datum, predmet, pocet);
        add(form);
    }

    /**
     * Ulozi data z UI komponentov do objektu pokial je validacia ok.
     *
     * @return True - vsetko je ok
     */
    private boolean uloz() {
        return binder.writeBeanIfValid(editBean);
    }

    /**
     * Vytvori container pre tlacidla a prida ho do editora
     */
    private void createButtonLayout() {
        buttonLayout = new HorizontalLayout();
        buttonLayout.setWidthFull();
        buttonLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
        add(buttonLayout);
    }

    /**
     * Prida tlacidlo s textom "Uloz"
     *
     * @param consumer Reakcia na stlacenie tlacidla
     * @return Tlacidlo "Uloz"
     */
    public Button addButtonUloz(Consumer<Objednavka> consumer) {
        Button btn = new Button("Ulož", event -> {
            if (uloz())
                consumer.accept(editBean);
        });
        btn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        if (buttonLayout == null)
            createButtonLayout();

        buttonLayout.add(btn);
        return btn;
    }

    /**
     * Prida tlacidlo s textom "Zatvor"
     *
     * @param consumer Reakcia na stlacenie tlacidla
     * @return Tlacidlo "Zatvor"
     */
    public Button addButtonZatvor(Consumer<Objednavka> consumer) {
        Button btn = new Button("Zatvor", event -> {
            consumer.accept(editBean);
        });

        if (buttonLayout == null)
            createButtonLayout();

        buttonLayout.add(btn);
        return btn;
    }

    /**
     * Zobrazi editor v dialogovom okne
     *
     * @param closeOnOutsideClick Priznak ci sa ma dialog zatvorit pri kliknuti mimo
     */
    public void open(boolean closeOnOutsideClick) {
        dialog = new Dialog();
        dialog.setCloseOnOutsideClick(closeOnOutsideClick);
        dialog.add(this);
        dialog.open();
    }

    /**
     * Zatvori dialogove okno
     */
    public void close() {
        if (dialog != null)
            dialog.close();
    }
}